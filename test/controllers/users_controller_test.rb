require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get users_url
    assert_response :success
  end

  test "should not get index unless signed in" do
    get users_url
    assert_response 302
  end

end
