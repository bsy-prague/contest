Rails.application.routes.draw do
  get 'users', to: 'users#index'

  devise_for :users
  root to: redirect('/cats')
  resources :cats do
    get 'search', on: :collection
    get 'star', on: :member
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
