# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(email: 'foo@bar.baz', password: 'meow_MEOW%2017', password_confirmation: 'meow_MEOW%2017')

Cat.create!(name: 'Micka', description: 'Micka the kočka', user_id: 1, stars: 7)
Cat.create!(name: 'Elza', description: 'Elza the kocour', user_id: 1, stars: 3)

Cat.where(name: 'Micka').update_all(picture: 'micka.jpg')
Cat.where(name: 'Elza').update_all(picture: 'elza.jpg')
