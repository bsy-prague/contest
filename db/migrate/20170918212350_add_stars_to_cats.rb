class AddStarsToCats < ActiveRecord::Migration
  def change
    add_column :cats, :stars, :integer, default: 0
  end
end
