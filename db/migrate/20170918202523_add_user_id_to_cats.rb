class AddUserIdToCats < ActiveRecord::Migration
  def change
    add_reference :cats, :user, foreign_key: true
  end
end
