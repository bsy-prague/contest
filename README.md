# README

Česká verze je níže.


Welcome to RailsGrills contest. We have prepared a rails app for you.
Your task is to try to run the app and find bugs. There are at least
10 bugs. Try to find as many as possible within time limit (10 minutes).

"Report" the bugs by creating responsible bug reports here:
https://railsgrills.net/contest-ostrava/

In case you have spare time, you can propose fixes to get plus points.



Vítejte v soutěži RailsGrills. Připravili jsme pro Vás rails aplikaci.
Vaším úkolem je pokusit se aplikaci rozběhnout a najít chyby. V aplikaci
je alespoň 10 chyb. Pokuste se jich najít co nejvíce v časovém limitu
10 minut.

Nalezené chyby hlašte zodpovědným vytvořením hlášení na adrese
https://railsgrills.net/contest-ostrava/

V případě, že vám bude zbývat čas, můžete navrhnout opravy chyb a získat
malé bezvýznamné plus.


Info

* Ruby version

2.4.1

* System dependencies

* Configuration

no configuration necessary

* Database creation

sqlite3

* Database initialization

rake db:migrate
rake db:seed

* How to run the test suite

rake

* Deployment instructions

run locally, via puma, rails s
