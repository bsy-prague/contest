json.extract! cat, :id, :name, :description, :picture, :created_at, :updated_at
json.url cat_url(cat, format: :json)
