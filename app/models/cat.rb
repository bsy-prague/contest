class Cat < ActiveRecord::Base
  mount_uploader :picture, PictureUploader
  belongs_to :user
  has_one :picture
end
