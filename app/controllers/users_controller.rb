class UsersController < ApplicationController
  def index
    @users = User.all
    respond_to do |format|
      format.html
      format.json { render json: @users.map{|u| u.attributes}.to_json }
    end
  end
end
