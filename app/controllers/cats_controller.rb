class CatsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cat, only: [:show, :edit, :update, :destroy, :star]

  # GET /cats
  # GET /cats.json
  def index
    @cats = Cat.all
  end

  def search
    @cats = Cat.where("name like '#{params[:term]}%'")
    render action: 'index'
  end

  def star
    @cat.stars = @cat.stars + 1
    @cat.save!
    redirect_to cats_path
  end

  # GET /cats/1
  # GET /cats/1.json
  def show
    view = params.fetch(:details, :show)
    render view
  end

  # GET /cats/new
  def new
    @cat = Cat.new
  end

  # GET /cats/1/edit
  def edit
  end

  # POST /cats
  # POST /cats.json
  def create
    @cat = Cat.new(cat_params)
    @cat.user = User.last

    respond_to do |format|
      if @cat.save
        format.html { redirect_to @cat, notice: 'Cat was successfully created.' }
        format.json { render :show, status: :created, location: @cat }
      else
        format.html { render :new }
        format.json { render json: @cat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cats/1
  # PATCH/PUT /cats/1.json
  def update
    respond_to do |format|
      make_backup @cat.picture.path, params[:cat][:picture].original_filename unless params[:cat][:picture].nil?
      if @cat.update(cat_params)
        format.html { redirect_to @cat, notice: 'Cat was successfully updated.' }
        format.json { render :show, status: :ok, location: @cat }
      else
        format.html { render :edit }
        format.json { render json: @cat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cats/1
  # DELETE /cats/1.json
  def destroy
    @cat.destroy
    respond_to do |format|
      format.html { redirect_to cats_url, notice: 'Cat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def make_backup file_name, orig_name
    data_path = Rails.root.join("public", "data")
    system("mkdir -p #{data_path}")
    system("cp #{file_name} #{data_path}/bak#{Time.zone.now.to_i}_#{orig_name}")
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_cat
    @cat = Cat.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cat_params
    params.require(:cat).permit(:name, :description, :picture, :user_id)
  end
end
